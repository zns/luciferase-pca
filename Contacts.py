import os
import math
import numpy as np
from multiprocessing import Pool

# 04/15/2016

# CUTOFF is how you determine a contact is formed, or broken.  CUTOFF=1.2 means
# a native contact is considered formed if the involved atoms are within 120%
# of their native distance.  
CUTOFF = 1.4

# TOP file is the topology file
TOPFILE = '1BA3_Model.9123.pdb.top'

inPairs = False
contacts = {}
with open(TOPFILE,'r') as f:
	for line in f:
		if inPairs and ';' not in line:
			info = line.split()
			if len(info) == 5:
				i = int(info[0])
				j = int(info[1])
				if i>j:
					i,j = j,i
				w1 = float(info[3])
				w2 = float(info[4])
				if i not in contacts:
					contacts[i] = {}
				print w2
				print w1
				contacts[i][j] = math.pow(6/5*w2/w1,0.5)*10.0

			else:
				inPairs = False
		if '[ pairs ]' in line:
			inPairs = True


numContacts = 0
for i in contacts.keys():
	for j in contacts[i].keys():
		numContacts += 1

def printArray(b):
  return '' + ' '.join([`b[i]` for i in xrange(len(b))]) + ''




def processFolder(folder):
	if 'eq_temp' in folder:
		print('working in ' + folder)
		os.chdir(folder)
		os.system('echo 0 | trjconv -skip 10 -s topol.tpr -o tmp.pdb -f traj_comp.xtc')
		pdb = {}
		num = 0
		contactNum = 0
		f1 = open('allcontacts.dat','w')
		f1.write('')
		f2 = open('frames.dat','w')
		f2.write('')
		f3 = open('contacts.dat','w') 
		f3.write('')
		f4 = open("0dumped.pdb","w")
		with open('tmp.pdb','r') as f:
			for line in f:
				isIntermediate = num % 1 == 0 and num > 0
				if 'MODEL' in line:
					if len(pdb.keys()) > 0 and isIntermediate:
						# generate contact map
						print("Generating contact map for " + folder + " on " + str(num))
						isContact = np.zeros(numContacts)
						ind = 0
						for i in contacts.keys():
							for j in contacts[i].keys():
								dist = np.linalg.norm(pdb[i]-pdb[j])
								isContact[ind] = dist < contacts[i][j]*CUTOFF
								if isContact[ind] > 0:
									contactNum += 1
								ind += 1
						f1.write(printArray(isContact) + '\n')
						f2.write( str(num) + '\n')
						f3.write(str(float(contactNum)/float(numContacts)) + '\n')
					pdb = {}
					num += 1
					f4.close()
					f4 = open(str(num) + "dumped.pdb","w")
					contactNum = 0
				info = line.split()
				if 'ATOM' == info[0]  and isIntermediate:
					x = float(line[31:39])
					y = float(line[39:47])
					z = float(line[47:56])
					pdb[int(info[1])] = np.array([x,y,z])
					f4.write(line.strip() + "\n")
		os.system('rm *tmp.pdb*')
		os.chdir('../')



folders = [x[0] for x in os.walk('./')]
print(folders)

p = Pool(64)
p.map(processFolder,folders)

'''LOOP THROUGH ALL CONTACTS
for i in pdb.keys():
	for j in pdb.keys():
		if i != j:
			print i,j,np.linalg.norm(pdb[i]-pdb[j])
'''
