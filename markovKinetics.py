import os

from matplotlib import pyplot
import matplotlib.cm as mplcm
import matplotlib.colors as colors
import numpy as np

P = np.load('markovMatrix.npy')
folder = 'clusters%d' % (len(P)-2)

for ii in range(len(P)):
    P = np.matrix(P)
    v = []
    for i in range(len(P)):
    	v.append(0)
    v[ii]=1
    v = np.array(v).reshape(1,len(P))

    # print(P)
    # Get the data
    plot_data = {}
    for i in range(len(P)):
    	plot_data[i] = []
    xx = range(0,500,5)
    thingsToKeep = []
    for step in xx:
        result = v * P**step
        for i in range(len(P)):
        	if result.item(0,i) > 0.0005:
        		thingsToKeep.append(i)

    thingsToKeep = list(set(thingsToKeep))
    for step in xx:
        result = v * P**step
        for i in range(len(thingsToKeep)):
        	plot_data[i].append(result.item(0,thingsToKeep[i]))
        
    # Create the plot
    NUM_COLORS = len(thingsToKeep)

    cm = pyplot.get_cmap('gist_rainbow')
    cNorm  = colors.Normalize(vmin=0, vmax=NUM_COLORS-1)
    scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)
    fig = pyplot.figure(1)
    ax = fig.add_subplot(111)
    ax.set_color_cycle([scalarMap.to_rgba(i) for i in range(NUM_COLORS)])
    pyplot.xlabel('Steps')
    pyplot.ylabel('Probability')
    for i in range(len(thingsToKeep)):
    	pyplot.plot(xx,plot_data[i],label='%d'%thingsToKeep[i])
    axes = pyplot.gca()
    pyplot.legend()
    pyplot.savefig(os.path.join(folder,'kineticsfrom%d.png'%ii))
    pyplot.close()
