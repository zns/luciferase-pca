# https://stackoverflow.com/questions/20133479/how-to-draw-directed-graphs-using-networkx-in-python

import csv
import time
import fnmatch
import os
import json
import operator

import numpy as np
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt

transitions = json.load(open('transitions.json','r'))
stateContacts = json.load(open('stateContacts.json','r'))

colorGradient = ['#E50400','#E12F00','#DD5900','#D98100','#D5A800','#D2CD00','#ABCE00','#81CA00','#59C600','#32C200','#0CBF00']

G = nx.DiGraph()
G = nx.Graph()


totalTransitions = {}
totalRate = {}
for i in transitions:
    waitTimes = []
    for j in transitions[i]:
        if i not in totalTransitions:
            totalTransitions[i] = 0
        totalTransitions[i] += len(transitions[i][j])
        waitTimes += transitions[i][j]
    totalRate[i] = 1000.0 / np.mean(waitTimes)


for i in transitions:
    for j in transitions[i]:
        if i!='-1' and len(transitions[i][j])>2:
            probabilityOfTransition = 100*len(transitions[i][j])/totalTransitions[i]
            rate = totalRate[i]
            flux = probabilityOfTransition*rate
            if flux > 1:
                G.add_edges_from([(i+'->',j)],weight=round(flux))
                print(i,j,np.mean(transitions[i][j]))





val_map = {}
for i in stateContacts:
    theColor = colorGradient[0]
    for k, color in enumerate(colorGradient):
        if stateContacts[i]*100 < k*10:
            theColor = color
            break
    if i == '-1':
        theColor = colorGradient[-1]
    if i == '-2':
        theColor = colorGradient[0]
    val_map[i] = theColor
    val_map[i+'->'] = theColor

colors = [val_map.get(node, 0.45) for node in G.nodes()]

edge_labels=dict([((u,v,),d['weight'])
                 for u,v,d in G.edges(data=True)])

# pos=nx.circular_layout(G)
# pos=nx.spectral_layout(G)
# pos=graphviz_layout(G,prog='dot')
pos=nx.spring_layout(G)
pos=nx.shell_layout(G)
plt.figure(figsize=(12,12))
nx.draw(G,pos, node_color=colors, node_size=1000)
nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)
node_labels = {node:node for node in G.nodes()}
nx.draw_networkx_labels(G, pos, labels=node_labels)
# plt.savefig('test.png')
plt.show()


# def weighted_graph_with_edge_labels():
#     #create an empty graph
#     G = nx.Graph()
    
#     #add three edges
#     G.add_edge('A','B')
#     G.add_edge('B','C')
#     G.add_edge('C','A')
#     G.add_edge('D','A')
#     G.add_edge('E','A')

#     G['A']['B']['weight'] = 10
#     G['B']['C']['weight'] = 14
#     G['C']['A']['weight'] = 25
#     G['D']['A']['weight'] = 50
#     G['E']['A']['weight'] = 700

#     # position the nodes by Force Layout
#     pos = nx.spring_layout(G)
    
#     #position the nodes according the output
#     # from the spring/force layout algorithm
#     nx.draw(G,pos)

#     #show the created graph
#     plt.show()
#     #shorter edge length indicates higher weight

#     edge_weight=dict([((u,v,),int(d['weight'])) for u,v,d in G.edges(data=True)])

#     nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_weight)
#     nx.draw_networkx_nodes(G,pos)
#     nx.draw_networkx_edges(G,pos)
#     nx.draw_networkx_labels(G,pos)
#     plt.axis('off')
#     plt.savefig('test.png')

# weighted_graph_with_edge_labels()

G = nx.DiGraph()

for i in transitions:
    for j in transitions[i]:
        if i!='-1' and len(transitions[i][j])>0:
            G.add_edges_from([(i,j)])




paths = {}
for path in nx.all_simple_paths(G, source='-2',target='-1'):
    waitingTime = 0
    for k in range(1,len(path)):
        i = path[k-1]
        j = path[k]
        probabilityOfTransition = 100*len(transitions[i][j])/totalTransitions[i]
        rate = totalRate[i]
        waitingTime += 100/(probabilityOfTransition*rate)
    if not np.isinf(waitingTime):
        paths[' '.join(path)] = waitingTime


sorted_x = sorted(paths.items(), key=operator.itemgetter(1))
for i,j in enumerate(sorted_x):
    print(str(j[0]),j[1])
