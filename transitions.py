#!/usr/bin/python3

import csv
import time
import fnmatch
import os
import json

import networkx as nx
import numpy as np

transitions = json.load(open('transitions.json','r'))



G = nx.DiGraph()

totalTransitions = {}
for i in transitions:
    for j in transitions[i]:
        if i!='-1' and len(transitions[i][j])>2:
            G.add_edges_from([(i,j)],weight=int(1000/round(np.mean(transitions[i][j]))))
        if i not in totalTransitions:
            totalTransitions[i] = 0
        totalTransitions[i] += len(transitions[i][j])

for path in nx.all_simple_paths(G, source='-2',target='-1'):
    flux = 0
    for i in range(1,len(path)):
        flux += len(transitions[path[i-1]][path[i]])/totalTransitions[path[i-1]]
    print(path,flux)

path = ['-2','2']
i=1
print( len(transitions[path[i-1]][path[i]])/totalTransitions[path[i-1]])
path = ['-2','0']
i=1
print( len(transitions[path[i-1]][path[i]])/totalTransitions[path[i-1]])


path = ['0','-2']
i=1
print( len(transitions[path[i-1]][path[i]])/totalTransitions[path[i-1]])

path = ['0','4']
i=1
print( len(transitions[path[i-1]][path[i]])/totalTransitions[path[i-1]])
