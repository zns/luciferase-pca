import csv
import time
import fnmatch
import os
import json

from tqdm import tqdm
import numpy as np
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.metrics.pairwise import pairwise_distances_argmin
from sklearn.metrics import silhouette_score

FILELIMIT = 3000

matches = []
for root, dirnames, filenames in os.walk('./'):
    for filename in fnmatch.filter(filenames, 'allcontacts.dat'):
        matches.append(os.path.join(root, filename))



# x = []
# ijk=0
# with open('filtered.out','w') as f:
#     for ij in tqdm(range(len(matches))):
#         match = matches[ij]
#         reader=csv.reader(open(match,"rb"),delimiter=' ',skipinitialspace=True)
#         for i,line in enumerate(list(reader)):
#             if len(line[-1]) == 0:
#                 continue
#             percentContacts = np.sum(np.array(line).astype('float'))/len(line)
#             if percentContacts > 0.2 and percentContacts < 0.9:
#                 x.append(line)
#                 f.write(' '.join(line) + "\n")
#         ijk+=1
#         if ijk > FILELIMIT:
#             break

# print("Saving data")
# X=np.array(x).astype('float')
# print(len(X))
# np.save(open('X.npy','w'),X)


print("Loading data")
X = np.load('X.npy')
batch_size = 45
range_n_clusters = range(10,20)

# for n_clusters in range_n_clusters:
#     # mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_clusters, batch_size=batch_size,
#     #                       n_init=10, max_no_improvement=10, verbose=0)
#     mbk = KMeans(n_clusters=n_clusters, random_state=10)
#     t0 = time.time()
#     mbk.fit(X)
#     silhouette_avg = silhouette_score(X,  mbk.labels_)
#     print("For n_clusters =", n_clusters,
#           "The average silhouette_score is :", silhouette_avg)

print("Calculating KMeans")
number_clusters = 30
mbk = KMeans(n_clusters=number_clusters, random_state=10)
mbk = MiniBatchKMeans(init='k-means++', n_clusters=number_clusters, batch_size=batch_size,
                      n_init=50, max_no_improvement=50, verbose=0)
t0 = time.time()
mbk.fit(X)
t_mini_batch = time.time() - t0
mbk_means_labels = mbk.labels_
np.savetxt('labels.out', mbk_means_labels, delimiter=',')
mbk_means_cluster_centers = mbk.cluster_centers_
mbk_means_labels_unique = np.unique(mbk_means_labels)
np.savetxt('clusters.out', mbk_means_cluster_centers, delimiter=',')
np.save(open('clusters.npy','w'), mbk_means_cluster_centers)


# Open the original data and assign clusters 
# simultaneously determine transitions
print("Calculating transitions and markov matrix")
transitions = {}
stateContacts = {}
markovMatrix = np.zeros([number_clusters+2, number_clusters+2])
currentState = -2
currentTime = 0
pos = 0
ijk=0
for ij in tqdm(range(len(matches))):
    match = matches[ij]
    folder = match.split('allcontacts.dat')[0]
    reader=csv.reader(open(match,"rb"),delimiter=' ',skipinitialspace=True)
    ijk+=1
    if ijk > FILELIMIT:
        break
    with open(os.path.join(folder,'alllabels.out'),'w') as f:
        isFirst = True
        for i,line in enumerate(list(reader)):
            if len(line[-1]) == 0:
                continue
            percentContacts = np.sum(np.array(line).astype('float'))/len(line)
            state = 0
            if percentContacts <= 0.2:
                state = 0
            elif percentContacts >= 0.9:
                state = number_clusters+1
            else:
                state = mbk_means_labels[pos]+1
                pos +=1
            f.write("%d\n" % currentState)
            if not isFirst:
                markovMatrix[currentState][state]+=1
            if state != currentState and not isFirst:
                currentStateName = str(currentState+2)
                newStateName = str(state+2)
                if currentStateName not in transitions:
                    stateContacts[currentStateName] = percentContacts
                    transitions[currentStateName] = {}
                if newStateName not in transitions[currentStateName]:
                    transitions[currentStateName][newStateName] = []
                transitions[currentStateName][newStateName].append(currentTime)
                currentTime = 0
            currentState = state
            currentTime += 1
            isFirst = False



print(markovMatrix)
row_sums = markovMatrix.sum(axis=1)
new_matrix = markovMatrix / row_sums[:, np.newaxis]

np.save(open('markovMatrix.npy','w'),new_matrix)

with open('transitions.json','w') as f:
    f.write(json.dumps(transitions))
with open('stateContacts.json','w') as f:
    f.write(json.dumps(stateContacts))
