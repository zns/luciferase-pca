import os
from graphviz import Digraph
import numpy as np
import matplotlib.pyplot as plt

clusters=np.load('clusters.npy')

folder = 'clusters%d' % len(clusters)

os.mkdir(folder)

for i in range(len(clusters)):
	plt.fill_between(range(len(clusters[i])),0,clusters[i],color='blue')
	plt.savefig(os.path.join(folder,'%d.png' % (i+1)))
	plt.close()

markovMatrix = np.load('markovMatrix.npy')
dot = Digraph(comment='Transitions')
print(markovMatrix)
for i in range(len(markovMatrix)):
	dot.node(str(i),str(i))
		
print(markovMatrix[0])
for i in range(len(markovMatrix)):
	for j in range(len(markovMatrix)):
		if markovMatrix[i][j] > 0.001 and i != j:
			dot.edge(str(i),str(j), label=str(round(markovMatrix[i][j],3)))
		
dot.render(os.path.join(folder,'markov.gv'), view=False)




